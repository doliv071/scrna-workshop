#!/usr/bin/R

#################################################################
# HELPER FUNCTIONS
#################################################################
# only show the head of each item in a list (list head)
lead <- function(x, n = 6){
    lapply(x, head, n = n)
}
# show a reasonable head for large matrices (big head)
bead <- function(x, n = 10){
    x[1:n,1:n]
}
# split a string into a matrix of components
strsplit2 <- function (x, split, ...) {
    x <- as.character(x)
    n <- length(x)
    s <- strsplit(x, split = split, ...)
    nc <- unlist(lapply(s, length))
    out <- matrix("", n, max(nc))
    for (i in 1:n) {
        if (nc[i]) 
            out[i, 1:nc[i]] <- s[[i]]
    }
    out
}
#################################################################
# FUNCTION
#   row_names is a function to replace the uninterpretable 
#   magrittr workaround for assigning rownames via pipes
#   this function replaces `rownames<-` (., value) 
# COMMENT
#    Because I can't figure out why no one has done this yet
# REQUIRES
#    
# VARIABLE
#    x = the data passed from a pipe (a dataframe?)
#    value = a vector of the rownames of x
# RETURNS
#    x with rownames = value
#################################################################
row_names <- function(x, value){
    stopifnot(NROW(x) == length(value))
    rownames(x) <- value
    return(x)
}

col_names <- function(x, value){
    stopifnot(NCOL(x) == length(value))
    colnames(x) <- value
    return(x)
}

#################################################################
# FUNCTION
#    
# COMMENT
#    
# REQUIRES
#    
# VARIABLE
#    
# RETURNS
#    
#################################################################
slingshotPseudotimeMarkers <- function(sce, slingshotTrajectory = "slingPseudotime_1",
                                       feature_set = "HVGs",
                                       returnPVals = TRUE, plot = TRUE,
                                       clusterLabels = "DBSCAN"){
    require(slingshot)
    require(clusterExperiment, quietly = T)
    require(gam, quietly = T)
    
    trajectory <- colData(sce)[, slingshotTrajectory]
    if(length(feature_set) == 1){
        varfeat <- rowData(sce)[, feature_set]
    } else {
        varfeat <- rownames(rowData(sce)) %in% feature_set
    }
    
    Y <- logcounts(sce)[varfeat,]
    # fit a GAM with a loess term for pseudotime
    gam.pval <- apply(Y, 1, function(z){
        d <- data.frame(z = z, t = trajectory)
        tmp <- gam(z ~ lo(t), data = d)
        p <- summary(tmp)[4][[1]][1,5]
        p
    })
    
    if(plot){
        checkClust <- sum(grepl(paste("^", clusterLabels, "$", sep = ""), colnames(colData(sce))))
        if(checkClust == 1){
            heatclus <- colData(sce)[ ,clusterLabels][order(trajectory, na.last = NA)]
        } else {
            stop("Cluster labels specified were not found in colData(sce)")
        }
        topgenes <- names(sort(gam.pval, decreasing = FALSE))[1:100]
        heatdata <- normcounts(sce)[rownames(normcounts(sce)) %in% topgenes, order(trajectory, na.last = NA)]
        ce <- ClusterExperiment(heatdata, heatclus, transformation = log1p)
        plotHeatmap(ce, clusterSamplesData = "orderSamplesValue",
                    visualizeData = 'transformed')
    }
    if(returnPVals){
        gam.padj <- p.adjust(gam.pval)
        return(data.frame(gam.pval = gam.pval, gam.padj = gam.padj))
    }
}



